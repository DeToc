<?php
/*
 * Copyright (c) 2015      Marcin Cieślak (saper.info) 
 * 
 * DeToc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DeToc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DeToc.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * DeToc extension
 *
 * @file
 * @ingroup Extensions
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 3.0 or later
 */

class DeToc {
	public static function RemoveToc($body, &$extracted_toc) {
		$extracted_toc = '';
		$last_toc_ptr = 0;
		$new_body = '';
		while ( preg_match( '/<div id="toc".*?>(.*)/s',
				$body, $toc_matches,
				PREG_OFFSET_CAPTURE, $last_toc_ptr ) === 1 ) {

			$toc_offset = $toc_matches[0][1];

			$rest = $toc_matches[1][0];
			$rest_ptr = $toc_matches[1][1];

			$divlevel = 1;
			$endtoc_ptr = $rest_ptr;
			while ( $divlevel > 0 && strlen($rest) > 0) {
				if ( preg_match(',<(/?)div.*?>(.*),s',
					 $rest, $div_matches,
					 PREG_OFFSET_CAPTURE ) === 1 ) {

					$rest = $div_matches[2][0];

					if ( $div_matches[1][0] === '/' ) {
						$divlevel = $divlevel - 1;
						if ( $divlevel > 0 ) {
							$endtoc_ptr += $div_matches[2][1];
						} else {
						    /* We skip the first <div>, so we need to skip the closing one */
							$endtoc_ptr += $div_matches[0][1];
						}
					} else {
						$divlevel = $divlevel + 1;
						$endtoc_ptr += $div_matches[2][1];
					}
				} else {
					$rest = '';
				}
			}
			$extracted_toc .= substr( $body, 
				$last_toc_ptr + $rest_ptr, $endtoc_ptr - $rest_ptr );

			$new_body .= substr( $body, $last_toc_ptr, $toc_offset ); 
			$last_toc_ptr += $endtoc_ptr;
		}
		$new_body .= substr( $body, $last_toc_ptr );
		return $new_body;
	}
}
